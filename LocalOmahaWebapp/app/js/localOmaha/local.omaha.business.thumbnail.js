'use strict';

var latlon = require('../latlon');

module.exports = function() {
    return {
        restrict: 'E',
        templateUrl: 'js/localOmaha/local.omaha.business.thumbnail.html',
        scope: {
            business: '=',
            userLocation: '=',
            userDeviceOrientation: '=',
            showCompass: '&'
        },
        controller: function($scope) {
            var businessLocation = {lat: $scope.business.lat, lon: $scope.business.lng};

            $scope.getDistance = function() {
                if ($scope.userLocation) {
                    $scope.units = 'miles';
                    $scope.business.distance = latlon.getDistanceFromCoords($scope.userLocation, businessLocation);
                    return $scope.business.distance;
                }
            };

            $scope.getDirection = function() {
                if ($scope.userLocation) {
                    $scope.business.showDirection = true;
                    $scope.business.direction = latlon.getBearingFromCoords($scope.userLocation, businessLocation) - $scope.userDeviceOrientation;
                    return 'rotate(' + $scope.business.direction + 'deg)';
                }
            };
        }
    };
};