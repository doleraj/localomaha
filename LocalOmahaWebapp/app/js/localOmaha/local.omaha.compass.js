'use strict';

var latlon = require('../latlon');

module.exports = function($scope, $route, $location, localOmahaUserLocation) {
    // meh, don't have a RESTful resource to query with ID, so just search the list
    $scope.list = $route.current.locals.list;
    for (var i=0; i<$scope.list.length; i++) {
        if ($scope.list[i].id == $route.current.pathParams.businessId) {
            $scope.business = $scope.list[i];
            break;
        }
    }

    var businessLocation = {lat: $scope.business.lat, lon: $scope.business.lng};

    $scope.getDistance = function() {
        if ($scope.userLocation) {
            $scope.units = 'miles';
            $scope.business.distance =  latlon.getDistanceFromCoords($scope.userLocation, businessLocation);
            return $scope.business.distance;
        }
    };

    $scope.getDirection = function() {
        if ($scope.userLocation) {
            $scope.business.showDirection = true;
            $scope.business.direction = latlon.getBearingFromCoords($scope.userLocation, businessLocation) - $scope.userDeviceOrientation;
            return $scope.business.direction;
        }
    };

    var userLocationWatchId = localOmahaUserLocation.watchUserLocation(setUserLocation, handleNoLocationError);

    function setUserLocation(position) {
        $scope.userLocation = {lat: position.coords.latitude,
            lon: position.coords.longitude};
        $scope.$apply();
    }

    function handleNoLocationError(error) {
        $log.error('geolocation.getCurrentPosition error: ', error);
    }

    function clearLocationWatch() {// TODO: will also want to clear watch when determined to be at the destination
        localOmahaUserLocation.clearWatch(userLocationWatchId, setUserDeviceOrientation);
    }

    localOmahaUserLocation.watchDeviceOrientation(setUserDeviceOrientation);

    function setUserDeviceOrientation(e) {
        $scope.userDeviceOrientation = (e.webkitCompassHeading || e.alpha);
        $scope.$apply();
    }

    $scope.$on('$destroy', clearLocationWatch);

    $scope.leaveCompassMode = function() {
        $location.url('/');
    };
};