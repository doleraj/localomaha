'use strict';

module.exports = function ($scope, $log, $location, localOmahaResource, localOmahaUserLocation) {
    $scope.sortOrder = 'name';
    $scope.filterBy = {};
    $scope.userDeviceOrientation = 0;

    localOmahaResource.getAllLocalOmahaBusinesses().$promise.then(
        function(businesses) {
            $scope.list = businesses;
        },
        function(error) {$log.error(error);}
    );

    var userLocationWatchId = localOmahaUserLocation.watchUserLocation(setUserLocation, handleNoLocationError);

    function setUserLocation(position) {
        $scope.userLocation = {lat: position.coords.latitude,
            lon: position.coords.longitude};
        $scope.$apply();
    }

    function handleNoLocationError(error) {
        $log.error('geolocation.getCurrentPosition error: ', error);
    }

    function clearLocationWatch() {// TODO: will also want to clear watch when determined to be at the destination
        localOmahaUserLocation.clearWatch(userLocationWatchId, setUserDeviceOrientation);
    }

    localOmahaUserLocation.watchDeviceOrientation(setUserDeviceOrientation);

    function setUserDeviceOrientation(e) {
        $scope.userDeviceOrientation = (e.webkitCompassHeading || e.alpha);
        $scope.$apply();
    }

    $scope.$on('$destroy', clearLocationWatch);

    $scope.showCompass = function (business) {
        $location.url('/local/' + business.id);
    };
};