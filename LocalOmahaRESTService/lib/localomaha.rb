require 'bundler'
require 'sinatra/base'
require 'sinatra/config_file'
require 'sinatra/cross_origin'
require 'sass'
require 'json'
require 'data_mapper'
require './api/init'
require './helpers/init'
require './models/init'

class LocalOmaha < Sinatra::Base
  register Sinatra::ConfigFile
  register Sinatra::CrossOrigin
  
  configure do
    set :app_file, __FILE__
    set :run, true
    set :server, %w[thin mongrel webrick]
    set :cookie_options, { path: '/' }
    set :sessions, true
    set :root, File.join(File.dirname(__FILE__), '../')
    enable :cross_origin
  end
  
  configure :development do
    require 'sinatra/reloader'
    register Sinatra::Reloader
    also_reload 'helpers/**/*.rb'
    also_reload 'models/**/*.rb'
    also_reload 'api/**/*.rb'
    set :raise_errors, true
    enable :cross_origin
    
    DataMapper::Logger.new($stdout, :debug)
  end
  
  # Load the user's config file
  set :environments, %w{process development production}
  config_file 'config/config.yml'
  
  # Database setup
  db_url = 'postgres://' + settings.db_user + ':' + settings.db_pass + '@' + settings.db_host + '/' + settings.db_name
  DataMapper.setup(:default, db_url)
  
  error do
    halt 500, 'Server error'
  end
  not_found do
    halt 404, 'File not found'
  end
end
