require 'sinatra/base'
require './helpers/init'
require './models/init'

# /api/business/:route
class BusinessApi < Sinatra::Base
  get '/:id' do
    business ||= Business.get(params[:id]) || halt(404)
    ResponseHelper.format_response(business, request.accept)
  end
end