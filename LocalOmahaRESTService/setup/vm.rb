# Note that all paths are relative to Vagrantfile
class VagrantConfig
  def configure(config)
    config.vm.provider :virtualbox do |virtualbox|
      virtualbox.customize ["modifyvm", :id, "--name", "lo_rest"]
    end
    
    config.vm.network :forwarded_port, guest: 9292, host: 3000
    config.vm.synced_folder '../../', '/src', :nfs => true
    config.vm.provision :shell, :path => '../vm.sh'
  end
end