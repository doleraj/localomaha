#!/bin/sh
# Set up the database
sudo -u pgsql dropdb localomaha
sudo -u pgsql dropuser lo_user
sudo -u pgsql createdb -T postgis_template localomaha
sudo -u pgsql psql -f /src/setup/localomaha.sql -d localomaha
ruby ./process.rb
ruby ./gis.rb
