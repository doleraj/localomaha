#!/usr/bin/env ruby
require 'pg'
require 'yaml'
require 'json'

# Read the config file
config = YAML.load_file('../config/config.yml')

# Set our constants
INPUTFILE = config['process']['input_file']
DATABASE = {
  'host' => config['process']['db_host'],
  'user' => config['process']['db_user'],
  'pass' => config['process']['db_pass'],
  'name' => config['process']['db_name']
}

# Connect to the database
db = PG.connect(:host => DATABASE['host'],
  :dbname => DATABASE['name'],
  :user => DATABASE['user'],
  :password => DATABASE['pass']
)
db.prepare('insert_business', 'insert into businesses(id, name, address, lat, lng, category, facebook, type, phone, url, store_url) values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11)')

JSON.create_id = ''
businesses = JSON.load(File.read(INPUTFILE))

businesses.each do |business|
  db.exec_prepared('insert_business', [
    business['id'],
    business['name'],
    business['address'],
    business['lat'],
    business['lng'],
    business['category'],
    business['facebook'],
    business['json_class'],
    business['phone'],
    business['url'],
    business['store_url']
  ])
end